#base imageを指定
FROM python:3.6.12-slim-stretch
MAINTAINER ytooyama

#コピーするファイルがあれば指定
#COPY static/ ./static
#COPY templates/ ./templates
#COPY flask-demoapp.py ./
COPY requirements.txt  ./
RUN pip3 install -r ./requirements.txt
